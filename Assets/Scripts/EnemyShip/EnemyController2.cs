﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController2 : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship2 enemySpaceship2;
        [SerializeField] private float chasingThresholdDistance;
        //[SerializeField] private PlayerSpaceship playerSpaceship;

        private PlayerSpaceship spawnedPlayerShip;
        
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship2.Fire();
        }

        private void MoveToPlayer()
        {
            // TODO: Implement this later

            spawnedPlayerShip = GameManager.Instance.spawnedPlayerShip;

            if(spawnedPlayerShip == null)
                return;
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);

            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship2.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }
        }
    }    
}
